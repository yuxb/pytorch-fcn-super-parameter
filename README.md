# find best lr for pytorch-fcn

[![PyPI Version](https://img.shields.io/pypi/v/torchfcn.svg)](https://pypi.python.org/pypi/torchfcn)
[![Build Status](https://travis-ci.org/wkentaro/pytorch-fcn.svg?branch=master)](https://travis-ci.org/wkentaro/pytorch-fcn)

Finding best learning rate implementation of [Fully Convolutional Networks](https://github.com/shelhamer/fcn.berkeleyvision.org).


## Requirements && Installation

Requirements and Installation see [pytorch-fcn](https://github.com/wkentaro/pytorch-fcn).


## Training

See [VOC example](torch-fcn/examples/voc).

## Preprocess

preprocess the imput data by /255,then -mean=[0.485,0.456,0.406],/std=[0.229,0.224,0.229]

## Accuracy

A best learning rate is 7.8e-10 and we found out the acc and mean iu is better than 1.0e-10 by test the val dataset.
